#!/usr/bin/env python3
"""Antiphishing flask service

To lint the file, install python3-pycodestyle, pylint, mypy, then:
    python /usr/lib/python3/dist-packages/pycodestyle.py \
        antiphishing_service.py
    pylint --rcfile .pylintrc antiphishing_service.py
    mypy antiphishing_service.py
"""

from typing import Union
from flask import Flask
import idna
import redis
import validators  # type: ignore
from antiphishing_config import config, SafeBrowsingList


r = redis.Redis(**config['redis'])  # type: ignore


def puny_convert(input_text: str) -> str:
    """Convert Punycode domains to Unicode
       Convert Unicode domains to Punycode

    Stolen from https://github.com/Yutsuro/punycode (MIT License)

    Keyword argument:
    input_text -- the string to convert
    """
    if not isinstance(input_text, str):
        raise ValueError("Input must be string.")
    if (input_text[:4] == "xn--") | (".xn--" in input_text):
        output_text = idna.decode(input_text)
    else:
        output_text = idna.encode(input_text).decode('utf-8')
    return output_text


def is_in_gsb(url: str) -> Union[bool, None]:
    """Check if the URL is in GSB

    Keyword argument:
    url -- the url to check
    """
    sbl = SafeBrowsingList(config['gsb_key'],
                           'google-safe-browsing.db')

    lists = sbl.lookup_url(url)
    if lists is not None:
        for i in lists:
            if str(i).find('WHITELIST') == -1:
                return True
        return None

    return False


def is_in_phishing_db(url: Union[str, None]) -> bool:
    """Check if the URL is in the Redis phishing DB

    Keyword argument:
    url -- the URL to check
    """
    if url is None:
        return False
    return len(r.hgetall(url)) != 0


def check_gsb_config(url: str) -> Union[dict[str, Union[str, bool]], None]:
    """Check that there is a GSB key in app config
    """
    if 'gsb_key' not in config:
        return {
            'url': url,
            'success': False,
            'error': 'No gsb_key setting in configuration file'
        }

    return None


def check_gsb_module(url: str) -> Union[dict[str, Union[str, bool]], None]:
    """Check that the gglsbl python module is present
    """
    if SafeBrowsingList is None:
        return {
            'url': url,
            'success': False,
            'error': 'The gglsbl python module is not installed'
        }

    return None


app = Flask(__name__)


@app.route("/<path:url>", methods=['GET'])
@app.route("/all/<path:url>", methods=['GET'])
@app.route("/v2/all/<path:url>", methods=['GET'])
@app.route("/v3/all/<path:url>", methods=['GET'])
def check_all(url: str):  # pylint: disable-msg=R0911
    """Return a JSON structure indicating if the given URL is in the
    database of phishing URLs or in the Google Safe Browsing database

    The HTTP status code gives also the result:
    200 -- phishing URL
    404 -- not a phishing URL
    400 -- not a valid URL
    501 -- there is a problem the Google Safe Browsing database: no API key
    """
    gsb_config = check_gsb_config(url)
    if gsb_config is not None:
        return (gsb_config, 501)

    gsb_module = check_gsb_module(url)
    if gsb_module is not None:
        return (gsb_module, 501)

    if validators.url(url):
        phishing = is_in_gsb(url)

        if phishing:
            return {
                'url': url,
                'success': True,
                'is_phishing': phishing
            }, 200 if phishing else 404

        # It’s in a GSB authorization list
        if phishing is None:
            return {
                'url': url,
                'success': True,
                'is_phishing': False
            }, 404

        phishing = is_in_phishing_db(url)

        return {
            'url': url,
            'success': True,
            'is_phishing': phishing
        }, 200 if phishing else 404

    return ({
        'url': url,
        'success': False,
        'error': f'{url} is not a valid URL'
    }, 400)


@app.route("/gsb/<path:url>", methods=['GET'])
@app.route("/v2/gsb/<path:url>", methods=['GET'])
@app.route("/v3/gsb/<path:url>", methods=['GET'])
def check_gsb(url: str):
    """Return a JSON structure indicating if the given URL is in the
    Google Safe Browsing database

    The HTTP status code gives also the result:
    200 -- phishing URL
    404 -- not a phishing URL
    400 -- not a valid URL
    501 -- there is a problem the Google Safe Browsing database: no API key
    """

    gsb_config = check_gsb_config(url)
    if gsb_config is not None:
        return (gsb_config, 501)

    gsb_module = check_gsb_module(url)
    if gsb_module is not None:
        return (gsb_module, 501)

    if validators.url(url):
        phishing = is_in_gsb(url)

        if phishing is not None:
            return {
                'url': url,
                'success': True,
                'is_phishing': phishing
            }, 200 if phishing else 404

        # It’s in a GSB authorization list
        return {
            'url': url,
            'success': True,
            'is_phishing': False
        }, 404

    return ({
        'url': url,
        'success': False,
        'error': f'{url} is not a valid URL'
    }, 400)


@app.route("/db/<path:url>", methods=['GET'])
@app.route("/v3/db/<path:url>", methods=['GET'])
def check_url(url: str):
    """Return a JSON structure indicating if the given URL is in the
    database of phishing URLs

    The HTTP status code gives also the result:
    200 -- phishing URL
    404 -- not a phishing URL
    400 -- not a valid URL
    """
    if validators.url(url):
        phishing = is_in_phishing_db(url)

        return {
            'url': url,
            'success': True,
            'is_phishing': phishing
        }, 200 if phishing else 404

    return ({
        'url': url,
        'success': False,
        'error': f'{url} is not a valid URL'
    }, 400)
