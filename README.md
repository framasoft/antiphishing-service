# Antiphishing service

## Phishing URL sources

- Phishstats: <https://phishstats.info/phish_score.csv> (Updated every 90 minutes with phishing URLs from the past 30 days.)
- Phishing database: <https://raw.githubusercontent.com/mitchellkrogza/Phishing.Database/master/ALL-phishing-links.tar.gz> (Updated every 2 hours)
- Phishtank: <https://data.phishtank.com/data/online-valid.json> (Updated hourly)
- Phishunt: <https://phishunt.io/feed.txt> (Continuously updated, in a loop)
- Google safe browsing: <https://safebrowsing.google.com/>

## Installation

### Dependencies

On Debian:

```bash
apt install python3-requests \
            python3-yaml \
            python3-redis \
            python3-flask \
            python3-validators \
            python3-idna \
            redis
```

Then just clone this repository somewhere.

An optional dependency is the python module `gglsbl`, if you want to use the Google Safe Browsing database.

Or you can install dependencies with `virtualenv` and `pip` (don’t forget to install a redis server):
```bash
virtualenv venv
. venv/bin/activate
pip install -r requirements.txt
```

## For development

Go to the repository directory and start the development server with:

```bash
flask --debug --app antiphishing_service run --host 0.0.0.0
```

### For production

Either deploy with your favorite method from <https://flask.palletsprojects.com/en/2.3.x/deploying/> or continue reading to deploy with [Waitress](https://docs.pylonsproject.org/projects/waitress/en/stable/).

On Debian:
```bash
apt install python3-waitress
```

```
cat <<EOF >> /etc/systemd/system/antiphishing-app.service
[Unit]
Description=Antiphishing service
Documentation=https://framagit.org/framasoft/antiphishing-service
Requires=network.target redis.service
After=network.target redis.service

[Service]
User=www-data
RemainAfterExit=yes
WorkingDirectory=/var/www/antiphishing-service
ExecStart=/usr/bin/waitress-serve --host=127.0.0.1 --port=5000 antiphishing_service:app

[Install]
WantedBy=multi-user.target
EOF
```

If you installed waitress in a `virtualenv`, replace it in `ExecStart` with its path (ex: `/var/www/antiphishing-service/venv/bin/waitress-serve`).

Of course, adapt the systemd service to your needs (user, working directory, host, port…)

```bash
systemctl daemon-reload
systemctl enable --now antiphishing-app.service
```

If you want to expose the service, use this for Nginx:
```
    merge_slashes off;
    location / {
        proxy_pass http://127.0.0.1:5000/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Prefix /;
    }
```

See <https://flask.palletsprojects.com/en/2.3.x/deploying/apache-httpd/> for Apache.

### Configuration

You can change the default configuration by creating a YAML file in `/etc/antiphishing-service.yml`:
```yaml
---
redis:
  host: distant-redis.example.org
exclusions:
  - .*\.example\.org
```

The default redis configuration is:
```yaml
redis:
  host: localhost
  port: 6379
  decode_responses: True
```

The redis configuration in `/etc/antiphishing-service.yml` will be merged with the default one.
Don’t change `decode_responses: True`!
See <https://redis.readthedocs.io/en/stable/examples.html> for connection examples.

The `exclusions` is an array of regular expressions which will be merged into one:
```
exclusions:
  - .*\.example\.org
  - foo\.bar\.org
```

will be transformed to `(.*\.example\.org|foo\.bar\.org)`.

The `gsb_key` setting is a [google safe browsing API key](https://console.cloud.google.com/apis/credentials), mandatory if you want to use GSB.
```yaml
gsb_key: GOOGLE_SAFE_BROWSING_API_KEY
```

## Populate the database

You need to fill the database before using the service.

The `populate_database.py` is here for that.
Discover how to use it with `./populate_database.py -h`.

Create a cron job to refresh the database regularly.

## API

### Testing domains

Domain testing have been removed from the app.

### Testing URLs against the GSB database

You just have to make a GET request on `/v3/gsb/<url>` or `/gsb/<url>`, where `<url>` is the URL you want to test.

You can test URLs from <https://testsafebrowsing.appspot.com/> to be sure that it works well.

### Testing URLs against the redis database

You just have to make a GET request on `/v3/db/<url>` or `/db/<url>`, where `<url>` is the URL you want to test.

### Testing URLs against the GSB and the redis databases

You just have to make a GET request on `/v3/all/<url>`, `/all/<url>` or `/<url>` where `<url>` is the URL you want to test.

### Version

The current version of the API is 3.

The root endpoint works as the last version of the API for testing against the GSB and redis databases.
In other words, you can do a GET request on `/<url>` and it will be the same as `/v3/all/<url>`

### Exemples

```
# API v3
curl http://127.0.0.1:5000/v3/db/https://test.example.org/example.html
curl http://127.0.0.1:5000/v3/gsb/https://test.example.org/example.html
curl http://127.0.0.1:5000/v3/all/https://test.example.org/example.html

# API v2
curl http://127.0.0.1:5000/v2/gsb/https://test.example.org/example.html
curl http://127.0.0.1:5000/v2/all/https://test.example.org/example.html
```

### Response

#### If the URL is listed in the GSB or in the redis databases

HTTP status code: 200

```json
{
  "url": "https://phishing.example.org",
  "is_phishing": true,
  "success": true
}
```

#### If the URL is safe

HTTP status code: 404

```json
{
  "url": "https://legit.example.org",
  "is_phishing": false,
  "success": true
}
```

#### If you tested something that is not an URL

HTTP status code: 400

```json
{
  "url": "not_a_url_example",
  "error": "not_a_url_example is not a valid url",
  "success": false
}
```

#### If there is no GSB key in the config file

HTTP status code: 501

```json
{
    "url": "the url",
    "success": False,
    "error": "No gsb_key setting in configuration file"
}
```

#### If the gglsbl python module is not installed

HTTP status code: 501

```json
{
    "url": "the url",
    "success": False,
    "error": "The gglsbl python module is not installed"
}
```
